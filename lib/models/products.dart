class Products {
  final String name;
  final double price;
  final String description;
  final String color;
  final int stock;

  Products.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        stock = json['stock'],
        price = double.parse( json['details']['price']),
        description = json['details']['description'],
        color = json['details']['color'];

  Products(this.name, this.price, this.color, this.description, this.stock);
}
