import 'package:mspr_project/models/products.dart';
import 'package:flutter/material.dart';
import 'package:mspr_project/ui/connect_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  late Products selectedProduct = Products("Opal Harris", 375.00, "violet",
      "The Football Is Good For Training And Recreational Purpose", 15979);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Authentification'),
          ),
          body: ConnectPage()
      ),
    );
  }
}
