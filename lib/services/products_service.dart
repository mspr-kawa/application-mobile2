import 'dart:convert';
import 'package:mspr_project/models/products.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ProductsService {
  static const apiUrl = 'https://api-revendeur.herokuapp.com/api/v1/products';

  static Future<List<dynamic>> getProducts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = await prefs.getString('token');
    try {
      final response = await http.get(Uri.parse(apiUrl), headers: {'Authorization' : 'Bearer ${token}'});
      if (response.statusCode == 200) {
        List<dynamic> list = json.decode(response.body)['products'];
        var details = list.map((e) => Products.fromJson(e)).toList();
        return details;

      } else {
        throw Exception('Erreur lors de la récupération des produits');
      }
    } catch (e) {
      throw Exception(e);
    }
  }

  static Future<Products> getOneProduct(String id) async{
    try {
      final response = await http.get(Uri.parse(apiUrl+ id.toString()) );
      if (response.statusCode == 200) {
        var produit = json.decode(response.body);
        return Products.fromJson(produit);
      } else {
        throw Exception('Erreur lors de la récupération des détails du produit');
      }
    } catch (e) {
      throw Exception(e);
    }

  }
}
