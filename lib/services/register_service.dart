import 'package:http/http.dart' as http;
import 'dart:convert';

class RegisterService{
  static const api = 'https://api-revendeur.herokuapp.com/api/v1/register';

  static Future<http.Response> sendAddressEmail(String email) async {

    final Map<String, dynamic> requestBody = {
      'email': email
    };

    final response = await http.post(Uri.parse(api),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
      body: jsonEncode(requestBody),
    );

    if(response.statusCode == 200){
      print('Response body : ${response.body}');
    }else{
      print('Resquest failed with status : ${response.statusCode}');
    }

    return response;
  }

}
