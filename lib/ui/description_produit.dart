import 'package:mspr_project/models/products.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './augmented_reality.dart';

class DescriptionProduit extends StatelessWidget {
  final Products prod;

  const DescriptionProduit({super.key, required this.prod});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails du produit'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text('Description :${prod.description}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              )),
          Text('Color : ${prod.color}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              )),
          Text('Prix :${prod.price}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              )),
          Text('En stock :${prod.stock}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              )),
          ElevatedButton(
              onPressed: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Augmented_reality()))
              },
              child: Text("Aperçu du produit"))
        ],
      ),
    );
  }
}
