import 'package:mspr_project/services/register_service.dart';
import 'package:mspr_project/ui/qr_code_scan.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class ConnectPage extends StatefulWidget {
  ConnectPage({Key? key}) : super(key: key);

  @override
  State<ConnectPage> createState() => _ConnectPageState();
}

class _ConnectPageState extends State<ConnectPage> {
  String errorMessage = '';
  String email = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomCenter,
            colors: [Colors.green.shade50, Colors.purple.shade50],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 70,
            ),
            SizedBox(
              width: 150,
              height: 150,
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                hintText: 'Adresse mail',
                border: OutlineInputBorder(),
                prefixIcon: Align(
                  widthFactor: 1.0,
                  heightFactor: 1.0,
                  child: Icon(Icons.email),
                ),
              ),
              keyboardType: TextInputType.emailAddress,
              onChanged: (val) {
                setState(() {
                  email = val;
                });
                validateEmail(val);
              },
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              errorMessage,
              style: TextStyle(
                color: Colors.red,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            OutlinedButton(
              child: Text('Recevoir email'),
              style: OutlinedButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.blue,
                fixedSize: Size.fromHeight(40),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
              ),
              onPressed: errorMessage.isEmpty
                  ? () {
                RegisterService.sendAddressEmail(email)
                    .then((value) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QrCodeScan(),
                    ),
                  );
                });
              }
                  : null,
            ),
          ],
        ),
      ),
    );
  }

  void validateEmail(String val) {
    if (val.isEmpty) {
      setState(() {
        errorMessage = "L'adresse e-mail ne peut pas être vide.";
      });
    } else if (!EmailValidator.validate(val)) {
      setState(() {
        errorMessage = "L'adresse e-mail n'est pas valide.";
      });
    } else {
      setState(() {
        errorMessage = "";
      });
    }
  }
}
