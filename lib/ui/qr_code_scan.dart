import 'package:mspr_project/ui/ListProduct.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QrCodeScan extends StatefulWidget {
  @override
  _QrCodeScanState createState() => _QrCodeScanState();
}

class _QrCodeScanState extends State<QrCodeScan> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  String qrData = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scanner QR Code"),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text("Résultat : $qrData"),
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) async{
    this.controller = controller;
    controller.scannedDataStream.listen((Barcode scanData) {
      setState(() {
        qrData = scanData.code!;
      });
      _sauvegarderVariable(qrData);
      controller.pauseCamera();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ListProduct()),
      );
    });
  }

  Future<void> _sauvegarderVariable(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}

