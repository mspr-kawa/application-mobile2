import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/products.dart';
import '../services/products_service.dart';
import 'description_produit.dart';


class ListProduct extends StatefulWidget {
  @override
  State<ListProduct> createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {
  //const ListProduct({Key? key}) : super(key: key);
  late Products selectedProduct;

  String? _token;


  @override
  void initState() {
    super.initState();
    _chargerVariable();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Liste des produits'),
        ),
        body: FutureBuilder(
          future: ProductsService.getProducts(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              final products = snapshot.data;
              return ListView.builder(
                itemCount: products?.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      products![index].name,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                            onPressed: () => {
                              selectedProduct = products[index],
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => DescriptionProduit(
                                      prod: selectedProduct)))
                            },
                            child: Text("Détails"))
                      ],
                    ),
                  );
                },
              );
            } else if (snapshot.hasError) {
              print("there is an errror with the snapshot");
              return Text(snapshot.error.toString());
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  Future<void> _chargerVariable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _token = prefs.getString('token');
      print("le token récupéré ${_token}");
    });
  }
}
